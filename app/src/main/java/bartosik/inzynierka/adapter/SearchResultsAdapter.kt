package bartosik.inzynierka.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import bartosik.inzynierka.R
import bartosik.inzynierka.SingleRideActivity
import bartosik.inzynierka.model.Ride
import bartosik.inzynierka.util.CircleTransformation
import bartosik.inzynierka.util.CommonTimeHelper.countCommonTime
import bartosik.inzynierka.util.CommonTimeHelper.printCommonTime
import com.squareup.picasso.Picasso
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class SearchResultsAdapter(private val list: ArrayList<Ride>, private val departureTime: Long, private val arrivalTime: Long) :
    RecyclerView.Adapter<SearchResultsAdapter.SearchResultsViewHolder>() {

    class SearchResultsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameText: TextView = itemView.findViewById(R.id.cardName)
        val timeText: TextView = itemView.findViewById(R.id.cardRideTime)
        val commonTimeText: TextView = itemView.findViewById(R.id.cardRideCommonTime)
        val descriptionText: TextView = itemView.findViewById(R.id.cardDescription)
        val cardImage: ImageView = itemView.findViewById(R.id.cardImage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_result_card_layout, parent, false)
        return SearchResultsViewHolder(
            view
        )
    }



    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: SearchResultsViewHolder, position: Int) {
        holder.commonTimeText.text = "Wspólny czas: " + printCommonTime(list[position], departureTime, arrivalTime)
        when(100*countCommonTime(list[position], departureTime, arrivalTime)/(list[position].arrivalTimestamp!!.seconds-list[position].departureTimestamp!!.seconds)){
            in 10..50 -> holder.commonTimeText.setTextColor(Color.parseColor("#FF0000"))
            in 50..80 -> holder.commonTimeText.setTextColor(Color.parseColor("#FFA500"))
            else -> holder.commonTimeText.setTextColor(Color.parseColor("#00FF00"))
        }
        holder.descriptionText.text = list[position].creatorDescription
        holder.nameText.text = list[position].creatorName
        holder.timeText.text = LocalDateTime.ofEpochSecond(list[position].departureTimestamp!!.seconds, 0, ZoneOffset.UTC).format(
            DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")) + " - " + LocalDateTime.ofEpochSecond(list[position].arrivalTimestamp!!.seconds, 0, ZoneOffset.UTC).format(
            DateTimeFormatter.ofPattern("dd.MM.yy HH:mm"))
        if (list[position].creatorPhoto != null) {

            Picasso.get().load(list[position].creatorPhoto).transform(CircleTransformation())
                .into(holder.cardImage)
        } else {
            Picasso.get().load(R.drawable.no_profile_photo).transform(CircleTransformation()).into(holder.cardImage)
        }

        holder.itemView.setOnClickListener {
            val i = Intent(holder.itemView.context, SingleRideActivity::class.java)
            i.putExtra("description", list[position].creatorDescription)
            i.putExtra("name", list[position].creatorName)
            i.putExtra("uri",list[position].creatorPhoto.toString())
            i.putExtra("uid", list[position].userId)
            i.putExtra("ride", holder.timeText.text.toString())
            i.putExtra("rideUid", list[position].uuid)
            holder.itemView.context.startActivity(i)
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }



}

