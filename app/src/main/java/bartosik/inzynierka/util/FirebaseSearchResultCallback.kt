package bartosik.inzynierka.util

import bartosik.inzynierka.model.Ride

interface FirebaseSearchResultCallback {
    fun onCallBack(rides: ArrayList<Ride>)
}