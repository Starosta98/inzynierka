package bartosik.inzynierka.util


import android.net.Uri
import android.util.Log
import android.view.MenuItem
import androidx.recyclerview.widget.RecyclerView
import bartosik.inzynierka.ChatToItem
import bartosik.inzynierka.ChatFromItem
import bartosik.inzynierka.R
import bartosik.inzynierka.model.*
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.Timestamp
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

object DatabaseManager {
    private var store: FirebaseFirestore = Firebase.firestore
    private var database: FirebaseDatabase = Firebase.database
    private const val RIDES_COLLECTION = "rides"
    private const val PROFILES_COLLECTION = "profiles"
    private const val MESSAGES_COLLECTION = "messages"
    private const val USER_MESSAGGES = "userMessages"
    private const val LATEST_MESSAGES = "latestMessages"


    fun addNewRide(ride: Ride): Task<DocumentReference> {
        return store.collection(RIDES_COLLECTION).add(ride)
    }

    fun getRideByUUID(uuid: String): Task<QuerySnapshot> {
        return store.collection(RIDES_COLLECTION).whereEqualTo("uuid", uuid).get()
    }

    fun getUserProfile(uid: String): DocumentReference {
        return store.collection("profiles").document(uid)
    }

    fun updateUserProfile(uid: String, description: String, photoUri: Uri?) {
        store.collection(PROFILES_COLLECTION).document(uid).update(
            mapOf(
                "description" to description,
                "photoUri" to photoUri.toString()
            )

        )
    }

    fun findRides(
        number: String,
        departure: Timestamp?,
        arrival: Timestamp?,
        callback: FirebaseSearchResultCallback
    ) {
        val rides = ArrayList<Ride>()
        val rides_two = ArrayList<Ride>()
        val firstTask = store.collection(RIDES_COLLECTION)
            .whereEqualTo("rideNumber", number)
            .whereLessThanOrEqualTo("departureTimestamp", arrival!!).get()
//            .addOnCompleteListener { task ->
//                if (task.isSuccessful) {
//                    for (ride in task.result!!) {
//                        rides.add(ride.toObject())
//                    }
//                    Log.d("XDD","Tu powinien być środek")
//                }
//            }
        val secondTask = store.collection(RIDES_COLLECTION)
            .whereEqualTo("rideNumber", number)
            .whereGreaterThanOrEqualTo("arrivalTimestamp", departure!!).get()
//            .addOnCompleteListener { secondTask ->
//                if (secondTask.isSuccessful) {
//                    for (ride in secondTask.result!!) {
//                        if (!rides.contains(ride.toObject())) {
//                            rides.add(ride.toObject())
//                        }
//                    }
//                    Log.d("XDD","Tu powinien być środek 2 ")
//
//                    callback.onCallBack(rides)
//                }
//            }

            Tasks.whenAllSuccess<QuerySnapshot>(firstTask, secondTask).addOnSuccessListener {
                for (ride in it[0]) {
                    rides.add(ride.toObject())
                }
                for (ride in it[1]) {
                    rides_two.add(ride.toObject())
                }
                rides.retainAll(rides_two)
                callback.onCallBack(rides)
            }
    }

    fun createNewProfile(uid: String) {
        store.collection(PROFILES_COLLECTION).document(uid).set(Profile())
    }

    fun saveMessage(message: Message){
//        store.collection(MESSAGES_COLLECTION).add(message)
//        store.collection(LATEST_MESSAGES).document(message.fromId).collection(LATEST_MESSAGES).document(message.toId).set(
//            mapOf(
//                "text" to message.text,
//                "timestamp" to message.timestamp,
//                "name" to toName,
//                "ride" to ride,
//                "uri" to toUri
//            )
//        )
//        store.collection(LATEST_MESSAGES).document(message.toId).collection(LATEST_MESSAGES).document(message.fromId).set(
//            mapOf(
//                "text" to message.text,
//                "timestamp" to message.timestamp,
//                "name" to fromName,
//                "ride" to ride,
//                "uri" to fromUri
//            )
//        )
        database.reference.child("$USER_MESSAGGES/${message.toId}/${message.fromId}").push().setValue(message)
        database.reference.child("$LATEST_MESSAGES/${message.toId}/${message.fromId}").setValue(message)
        message.seen = true
        database.reference.child("$USER_MESSAGGES/${message.fromId}/${message.toId}").push().setValue(message)
        database.reference.child("$LATEST_MESSAGES/${message.fromId}/${message.toId}").setValue(message)

    }

    fun messageListener(fromId: String, toId: String, adapter: GroupAdapter<GroupieViewHolder>, recyclerView: RecyclerView){

        database.reference.child("$USER_MESSAGGES/${fromId}/${toId}").addChildEventListener(
            object : ChildEventListener{
                override fun onCancelled(error: DatabaseError) {

                }

                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                }

                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                }

                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    val message = snapshot.getValue(Message::class.java)
                    if (message != null){
                        if (message.fromId == fromId){
                            adapter.add(ChatFromItem(message.text, fromId))
                        }else{
                            database.reference.child("$USER_MESSAGGES/${fromId}/${toId}/${snapshot.key}").updateChildren(
                                mapOf(
                                    "seen" to true
                                ))
                            database.reference.child("$LATEST_MESSAGES/${fromId}/${toId}").updateChildren(
                                mapOf(
                                    "seen" to true
                                ))
                            adapter.add(ChatToItem(message.text, toId))

                        }

                        recyclerView.scrollToPosition(adapter.itemCount - 1)

                    }
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                }

            }
        )
//        store.collection(MESSAGES_COLLECTION)
//            .whereEqualTo("fromId", fromId)
//            .whereEqualTo("toId", toId)
//            .orderBy("timestamp", Query.Direction.ASCENDING)
//            .addSnapshotListener{ snapshot, e ->
//                if (e != null) {
//                    Log.w("XDDD", "Listen failed.", e)
//                    return@addSnapshotListener
//                }
//                if (snapshot != null) {
//                    for (dc in snapshot.documentChanges) {
//                        if (dc.type == DocumentChange.Type.ADDED) {
//                            val message = dc!!.document.toObject<Message>()
//                            getUserProfile(fromId).get().addOnCompleteListener {
//                                val profile = it.result!!.toObject<Profile>()
//                                adapter.add(ChatFromItem(message.text, profile!!.photoUri))0
//                            }
//                        }
//                    }
//                }
//            }
//        store.collection(MESSAGES_COLLECTION)
//            .whereEqualTo("fromId", toId)
//            .whereEqualTo("toId", fromId)
//            .addSnapshotListener{ snapshots, _ ->
//                for (dc in snapshots!!.documentChanges){
//                    if (dc.type == DocumentChange.Type.ADDED){
//                        val message = dc!!.document.toObject<Message>()
//                        getUserProfile(toId).get().addOnCompleteListener {
//                            val profile = it.result!!.toObject<Profile>()
//                            adapter.add(ChatToItem(message.text, profile!!.photoUri))
//                        }
//                    }
//                }
//            }
    }

    fun endChat(fromId: String, toId: String){
        database.reference.child("$LATEST_MESSAGES/${fromId}/${toId}").removeValue()
        database.reference.child("$LATEST_MESSAGES/${toId}/${fromId}").removeValue()
    }

    fun acceptUser(rideUUID: String, acceptedUserId: String){
        getRideByUUID(rideUUID).addOnSuccessListener {
            val ride = it.documents[0].toObject<Ride>()!!
            store.collection(RIDES_COLLECTION).document(it.documents[0].id).update(
                "listed", false
            )
            getUserProfile(acceptedUserId).update("myRides", FieldValue.arrayUnion(it.documents[0].id))

        }
    }

    fun checkIfAnyNewMessages(fromId: String, bottomNavigationView: BottomNavigationView){
        val badge = bottomNavigationView.getOrCreateBadge(R.id.chat)
        database.reference.child("latestMessages/$fromId").addChildEventListener(object : ChildEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.d("XDDD", error.message)
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("XDDD", "Data " + snapshot.children.toString())
                badge.isVisible = snapshot.hasChildren()
                badge.number = snapshot.childrenCount.toInt()
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                badge.isVisible = snapshot.hasChildren()
                badge.number = snapshot.childrenCount.toInt()
//                val message = snapshot.getValue(Message::class.java)
//                if ()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                Log.d("XDDD", "Count " + snapshot.childrenCount.toString())
                badge.isVisible = snapshot.hasChildren()
                badge.number = snapshot.childrenCount.toInt()
            }

        })
    }

    fun getHistory(uid: String, myHistory: Boolean, adapter: GroupAdapter<GroupieViewHolder>){
        getUserProfile(uid).get().addOnSuccessListener {
            val profile = it.toObject<Profile>()
            val array = if (myHistory){
                profile!!.createdRides
            }else{
                profile!!.myRides
            }
            Log.d("XDDD", "Tu też jestem")
            for(ride in array){
                store.collection(RIDES_COLLECTION).document(ride).get().addOnSuccessListener { snap ->
                    val rideObj = snap.toObject<Ride>()!!
                    adapter.add(HistoryItem(rideObj.rideNumber!!, LocalDateTime.ofEpochSecond(rideObj.departureTimestamp!!.seconds, 0, ZoneOffset.UTC).format(
                        DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")) + " - " + LocalDateTime.ofEpochSecond(rideObj.arrivalTimestamp!!.seconds, 0, ZoneOffset.UTC).format(
                        DateTimeFormatter.ofPattern("dd.MM.yy HH:mm"))))
                }
            }
        }
    }

}
