package bartosik.inzynierka.util

import bartosik.inzynierka.model.Ride

object CommonTimeHelper {

    fun countCommonTime(rideOne: Ride, arrivalTime: Long, departureTime: Long): Long{
        return if (rideOne.arrivalTimestamp!!.seconds >= arrivalTime && rideOne.departureTimestamp!!.seconds <= departureTime ){
            arrivalTime - departureTime
        } else if (rideOne.arrivalTimestamp!!.seconds <= arrivalTime && rideOne.departureTimestamp!!.seconds <= departureTime ){
            rideOne.arrivalTimestamp!!.seconds - departureTime
        } else if (rideOne.arrivalTimestamp!!.seconds >= arrivalTime && rideOne.departureTimestamp!!.seconds >= departureTime ){
            arrivalTime - rideOne.departureTimestamp!!.seconds
        } else {
            rideOne.arrivalTimestamp!!.seconds - rideOne.departureTimestamp!!.seconds
        }
    }

    fun printCommonTime(ride: Ride, arrivalTime: Long, departureTime: Long): String{
        val seconds = countCommonTime(ride, arrivalTime, departureTime)
        return (seconds/3600).toString() + " h " + (seconds%3600)/60 + " m"
    }
}