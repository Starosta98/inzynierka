package bartosik.inzynierka

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import bartosik.inzynierka.fragment.AddFragment
import bartosik.inzynierka.fragment.ChatFragment
import bartosik.inzynierka.fragment.ProfileFragment
import bartosik.inzynierka.fragment.SearchFragment
import bartosik.inzynierka.util.DatabaseManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = Firebase.auth
        val profileFragment = ProfileFragment.newInstance()
        val chatFragment = ChatFragment.newInstance()
        val searchFragment = SearchFragment.newInstance()
        val addFragment = AddFragment.newInstance()
        setSupportActionBar(findViewById(R.id.toolbar))
        val actionBar = supportActionBar!!
        openFragment(searchFragment)
//        if (intent.extras!=null){
//            if (intent.extras!!.getBoolean("isNewPhoto")){
//                val fragment = supportFragmentManager.findFragmentByTag(ProfileFragment::class.java.toString())!!
//                supportFragmentManager.beginTransaction().remove(fragment).attach(fragment).commit()
//            }
//        }

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.chat -> {
                    // Respond to navigation item 1 click
                    actionBar.title = "Chat"
                    openFragment(chatFragment)
                }
                R.id.profile -> {
                    // Respond to navigation item 2 click
                    actionBar.title = "Profil"
                    openFragment(profileFragment)
                }
                R.id.search -> {
                    // Respond to navigation item 2 click
                    actionBar.title = "Wyszukaj przejazd"
                    openFragment(searchFragment)
                }
                R.id.add -> {
                    actionBar.title = "Dodaj przejazd"
                    openFragment(addFragment)
                }
            }
            true
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v: View? = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm: InputMethodManager =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onResume() {
        super.onResume()
//        DatabaseManager().checkIfAnyNewMessages(auth.uid!!, bottom_navigation)
    }

    private fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, fragment)
            detach(fragment)
            attach(fragment)
            commit()

        }
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            val a = Intent(Intent.ACTION_MAIN)
            a.addCategory(Intent.CATEGORY_HOME)
            a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(a)
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Kliknij POWRÓT jeszcze raz, by zamknąć aplikację", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        Log.d("XDDD", "Tu jest")
//        if (resultCode == Activity.RESULT_OK && requestCode == 1001){
//            Log.d("XDDD", "Tu też")
//            val fragment = supportFragmentManager.findFragmentByTag(ProfileFragment::class.java.toString())!!
//            supportFragmentManager.beginTransaction().remove(fragment).attach(fragment).commit()
//        }
//    }
}