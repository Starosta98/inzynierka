package bartosik.inzynierka.model

import bartosik.inzynierka.R
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.card_history.view.*

class HistoryItem(val number: String, val ride: String) : Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.card_history
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.trainNumberList.text = number
        viewHolder.itemView.ride.text = ride
    }

}