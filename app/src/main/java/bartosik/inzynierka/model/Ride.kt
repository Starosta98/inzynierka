package bartosik.inzynierka.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.Timestamp

class Ride(): Parcelable {

    constructor(uuid:String, rideNumber: String, departureTimestamp: Timestamp, arrivalTimestamp: Timestamp, creatorName: String?, creatorDescription: String?, creatorPhoto: String?, userId: String, listed: Boolean = true) : this() {
        this.rideNumber = rideNumber
        this.departureTimestamp = departureTimestamp
        this.arrivalTimestamp = arrivalTimestamp
        this.creatorDescription = creatorDescription
        this.creatorName = creatorName
        this.creatorPhoto = creatorPhoto
        this.userId = userId
        this.uuid = uuid
        this.listed = listed
    }

    var rideNumber: String? = null
    var departureTimestamp: Timestamp? = null
    var arrivalTimestamp: Timestamp? = null
    var creatorName: String? = null
    var creatorDescription: String? =null
    var creatorPhoto: String? = null
    var userId: String? = null
    var uuid: String? = null
    var listed: Boolean = false

    constructor(parcel: Parcel) : this() {
        rideNumber = parcel.readString()
        departureTimestamp = parcel.readParcelable(Timestamp::class.java.classLoader)
        arrivalTimestamp = parcel.readParcelable(Timestamp::class.java.classLoader)
        creatorName = parcel.readString()
        creatorDescription = parcel.readString()
        creatorPhoto = parcel.readString()
        userId = parcel.readString()
        uuid = parcel.readString()
        listed = parcel.readString()!!.toBoolean()
    }

    override fun equals(other: Any?): Boolean {
        val otherRide = (other as Ride)
        if (otherRide.rideNumber == rideNumber && otherRide.departureTimestamp == departureTimestamp && otherRide.arrivalTimestamp == arrivalTimestamp && otherRide.creatorDescription == creatorDescription && otherRide.creatorName == creatorName && otherRide.creatorPhoto == creatorPhoto){
            return true
        }
        return false
    }

    override fun hashCode(): Int {
        var result = rideNumber?.hashCode() ?: 0
        result = 31 * result + (departureTimestamp?.hashCode() ?: 0)
        result = 31 * result + (arrivalTimestamp?.hashCode() ?: 0)
        result = 31 * result + (creatorName?.hashCode() ?: 0)
        result = 31 * result + (creatorDescription?.hashCode() ?: 0)
        result = 31 * result + (creatorPhoto?.hashCode() ?: 0)
        result = 31 * result + (userId?.hashCode() ?: 0)
        result = 31 * result + (uuid?.hashCode() ?: 0)
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(rideNumber)
        parcel.writeParcelable(departureTimestamp, flags)
        parcel.writeParcelable(arrivalTimestamp, flags)
        parcel.writeString(creatorName)
        parcel.writeString(creatorDescription)
        parcel.writeString(creatorPhoto)
        parcel.writeString(userId)
        parcel.writeString(uuid)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Ride> {
        override fun createFromParcel(parcel: Parcel): Ride {
            return Ride(parcel)
        }

        override fun newArray(size: Int): Array<Ride?> {
            return arrayOfNulls(size)
        }
    }
}