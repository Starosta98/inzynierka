package bartosik.inzynierka.model

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.util.Log
import bartosik.inzynierka.ChatLogActivity
import bartosik.inzynierka.R
import bartosik.inzynierka.util.CircleTransformation
import bartosik.inzynierka.util.DatabaseManager
import com.google.firebase.Timestamp
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.latest_message.view.*
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class LatestMessageRow(val message: Message) : Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.latest_message
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {

        val chatPartnerId: String = if (message.fromId == Firebase.auth.uid){
            message.toId
        }else{
            message.fromId
        }
        DatabaseManager.getUserProfile(chatPartnerId).get().addOnSuccessListener {
            val profile = it.toObject<Profile>()!!
            viewHolder.itemView.setOnClickListener {
                val i = Intent(
                    viewHolder.itemView.context,
                    ChatLogActivity::class.java
                )
                i.putExtra("ride", message.ride)
                i.putExtra("name", profile.name)
                i.putExtra("uid", chatPartnerId)
                i.putExtra("uri", profile.photoUri)
                i.putExtra("rideUid", message.rideUid)
                viewHolder.itemView.context.startActivity(i)
            }
            viewHolder.itemView.latestMessageName.text = profile.name
            viewHolder.itemView.latestMessageRide.text = message.ride
            if (!message.seen){
                viewHolder.itemView.latestMessageText.setTypeface(null, Typeface.BOLD)
            }
            viewHolder.itemView.latestMessageText.text = message.text

            viewHolder.itemView.latestMessageTime.text =
                LocalDateTime.ofEpochSecond(
                    message.timestamp,
                    0,
                    ZoneOffset.UTC
                )
                    .format(
                        DateTimeFormatter.ofPattern("d.M.y h:m")
                    )
            if (profile.photoUri != null) {
                Picasso.get()
                    .load(Uri.parse(profile.photoUri)).transform(CircleTransformation())
                    .into(viewHolder.itemView.latestMessageImage)

            } else {
                Picasso.get()
                    .load(R.drawable.no_profile_photo).transform(
                        CircleTransformation()
                    )
                    .into(viewHolder.itemView.latestMessageImage)

            }
        }

    }


}