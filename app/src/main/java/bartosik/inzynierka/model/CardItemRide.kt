package bartosik.inzynierka.model

import android.net.Uri

class CardItemRide(val name: String?, val time: String?, val commonTime: String?,  val userDescription: String?, val userPhoto: Uri? = null, val userId: String){

}
