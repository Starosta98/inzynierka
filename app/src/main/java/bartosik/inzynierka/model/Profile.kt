package bartosik.inzynierka.model

import android.net.Uri


class Profile() {
    var name: String = ""
    var description: String = ""
    val tags: ArrayList<String> = ArrayList()
    val createdRides: ArrayList<String> = ArrayList()
    val myRides: ArrayList<String> = ArrayList()
    val photoUri: String? = null
}