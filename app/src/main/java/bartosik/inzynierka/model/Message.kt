package bartosik.inzynierka.model

import com.google.firebase.Timestamp

class Message() {
    var fromId: String = ""
    var toId: String = ""
    var text: String = ""
    var timestamp: Long = -1
    var ride: String = ""
    var rideUid: String = ""
    var seen: Boolean = false

    constructor(
        fromId: String,
        toId: String,
        text: String,
        timestamp: Long,
        ride: String,
        rideUid: String,
        seen: Boolean = false
    ) : this() {
        this.fromId = fromId
        this.toId = toId
        this.text = text
        this.timestamp = timestamp
        this.ride = ride
        this.rideUid = rideUid
        this.seen = seen
    }

    constructor(builder: Builder) : this() {
        fromId = builder.fromId
        toId = builder.toId
        text = builder.text
        timestamp = builder.timestamp
        ride = builder.ride
        rideUid = builder.rideUid
        seen = builder.seen
    }

    class Builder() {
        var fromId: String = ""
        var toId: String = ""
        var text: String = ""
        var timestamp: Long = -1
        var ride: String = ""
        var rideUid: String = ""
        var seen: Boolean = false

        fun setFromId(fromId: String): Builder {
            this.fromId = fromId
            return this
        }

        fun setToId(toId: String): Builder {
            this.toId = toId
            return this
        }

        fun setText(text: String): Builder {
            this.text = text
            return this
        }

        fun setTimestamp(timestamp: Long): Builder {
            this.timestamp = timestamp
            return this
        }

        fun setRide(ride: String): Builder{
            this.ride = ride
            return this
        }

        fun setRideUid(rideUid: String): Builder {
            this.rideUid = rideUid
            return this
        }

        fun setSeen(seen: Boolean): Builder {
            this.seen = seen
            return this
        }

        fun create(): Message {
            return Message(this)
        }

    }
}
