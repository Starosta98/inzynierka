package bartosik.inzynierka

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.text.Editable
import android.util.Patterns
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import bartosik.inzynierka.util.DatabaseManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        auth = Firebase.auth
        buttonRegister.setOnClickListener {
            var correct = true
            if (nameRegister.editText!!.text.toString().trim().isEmpty()) {
                nameRegister.error = "Pole wymagane"
                correct = false
            } else {
                nameRegister.error = null
            }
            if (!emailRegister.editText!!.text.toString().trim().isValidEmail()) {
                emailRegister.error = "Niepoprawny email"
                correct = false
            } else {
                emailRegister.error = null
            }
            if (!passwordRegister.editText!!.text.isPasswordValid()) {
                passwordRegister.error = "Hasło musi mieć przynajmniej 6 znaków"
                correct = false
            } else {
                passwordRegister.error = null
            }
            if (correct) {
                createNewUser()
            }

        }
    }

    private fun createNewUser() {
        val email = emailRegister.editText!!.text.toString().trim()
        val password = passwordRegister.editText!!.text.toString()
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = auth.currentUser
                    val intent = Intent(this, EditProfileActivity::class.java)
                    intent.putExtra("afterRegistration", true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    val profileUpdates = userProfileChangeRequest {
                        displayName = nameRegister.editText!!.text.toString()
                    }
                    user!!.updateProfile(profileUpdates)
                    auth.signInWithEmailAndPassword(email, password)
                        .addOnSuccessListener {
                            Toast.makeText(this,"Utworzono konto", Toast.LENGTH_LONG).show()
                            DatabaseManager.createNewProfile(auth.currentUser!!.uid)
                            startActivity(intent)
                            this.finish()
                        }
                } else {
                    // If sign in fails, display a message to the user.
                    emailRegister.error = "Email już jest w użyciu"
                }

                // ...
            }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v: View? = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm: InputMethodManager =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    private fun Editable?.isPasswordValid(): Boolean = this != null && this.length >= 6

    private fun String.isValidEmail(): Boolean =
        this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
}