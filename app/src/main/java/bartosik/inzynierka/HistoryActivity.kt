package bartosik.inzynierka

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import bartosik.inzynierka.util.DatabaseManager
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_history.toolbar
import kotlinx.android.synthetic.main.activity_single_ride.*

class HistoryActivity : AppCompatActivity() {
    val adapter = GroupAdapter<GroupieViewHolder>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        DatabaseManager.getHistory(Firebase.auth.uid!!, true, adapter)
        historyRecycler.adapter = adapter
        historyRecycler.hasFixedSize()
        toolbar.setNavigationOnClickListener {
            finish()
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab!!.position == 0){
                    Log.d("XDDD", "Add")
                    adapter.clear()
                    DatabaseManager.getHistory(Firebase.auth.uid!!, true, adapter)
                }
                if (tab.position == 1){
                    Log.d("XDDD", "Search")
                    adapter.clear()
                    DatabaseManager.getHistory(Firebase.auth.uid!!, false, adapter)
                }
            }

        })
    }
}