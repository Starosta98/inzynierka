package bartosik.inzynierka

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import bartosik.inzynierka.util.CircleTransformation
import bartosik.inzynierka.util.DatabaseManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.toolbar
import kotlinx.android.synthetic.main.activity_single_ride.*
import java.io.ByteArrayOutputStream

class EditProfileActivity : AppCompatActivity() {

    //image pick code
    private val IMAGE_PICK_CODE = 1000;
    private lateinit var storage: FirebaseStorage
    private lateinit var auth: FirebaseAuth
    private var uri: Uri? = null

    //Permission code
    private val PERMISSION_CODE = 1001;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        if (intent.extras!!.getBoolean("afterRegistration", false)) {
            toolbar.navigationIcon = null
            toolbar.title = "Stwórz profil"
        }
        toolbar.setNavigationOnClickListener {
            finish()
        }
        storage = Firebase.storage
        auth = Firebase.auth
        editName.editText!!.text.append(auth.currentUser!!.displayName)
        storage.reference.child("profile/" + auth.currentUser!!.uid).downloadUrl.addOnSuccessListener {
            Picasso.get().load(it).transform(CircleTransformation()).into(editPhoto)
        }.addOnFailureListener {
            Picasso.get().load(R.drawable.no_profile_photo).transform(CircleTransformation())
                .into(editPhoto)
        }

        editPhotoButton.setOnClickListener {
            //check runtime permission
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED
            ) {
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE);
            } else {
                //permission already granted
                pickImageFromGallery();
            }
        }

        buttonEditSave.setOnClickListener {
            val ref = storage.reference.child("profile/" + auth.currentUser!!.uid)
            val bitmap = (editPhoto.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()
            ref.putBytes(data)

            val user = auth.currentUser!!

            getPhotoUrl(object : FirestoreCallback {
                override fun onCallback(uri: Uri?) {
                    val profileUpdates = userProfileChangeRequest {
                        displayName = editName.editText!!.text.toString()
                        photoUri = uri
                    }
                    user.updateProfile(profileUpdates).addOnCompleteListener {
                        DatabaseManager.updateUserProfile(user.uid, editDescription.editText!!.text.toString(), uri)

                        val intent = Intent(this@EditProfileActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            })



        }
    }

    private fun getPhotoUrl(firestoreCallback: FirestoreCallback){
        val ref = storage.reference.child("profile/" + auth.currentUser!!.uid)
        ref.downloadUrl.addOnCompleteListener {
            var photoUri: Uri? = null
            if (it.isSuccessful){
                photoUri = it.result
            }
            firestoreCallback.onCallback(photoUri)
        }
    }



    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }


    //handle requested permission result
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            uri = data?.data
            Picasso.get().load(data?.data).transform(CircleTransformation()).into(editPhoto)
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v: View? = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm: InputMethodManager =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }


    private interface FirestoreCallback {
        fun onCallback(uri: Uri?);
    }

}