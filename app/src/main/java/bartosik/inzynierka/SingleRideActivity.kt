package bartosik.inzynierka

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import bartosik.inzynierka.util.CircleTransformation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_single_ride.*

class SingleRideActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_ride)
        toolbar.setNavigationOnClickListener {
            finish()
        }
        singleRideDescription.text = intent.getStringExtra("description")
        singleRideName.text = intent.getStringExtra("name")
        if (intent.getStringExtra("uri") != null) {
            Picasso.get().load(Uri.parse(intent.getStringExtra("uri"))).transform(
                CircleTransformation()
            ).into(singleRideImage)
        }else{
            Picasso.get().load(R.drawable.no_profile_photo).transform(CircleTransformation()).into(singleRideImage)
        }
        buttonNewChat.setOnClickListener {
            val i = Intent(this, ChatLogActivity::class.java)
            i.putExtra("ride", intent.getStringExtra("ride"))
            i.putExtra("name", intent.getStringExtra("name"))
            i.putExtra("uid", intent.getStringExtra("uid"))
            i.putExtra("uri", intent.getStringExtra("uri"))
            i.putExtra("rideUid", intent.getStringExtra("rideUid"))
            startActivity(i)
            finish()
        }
    }
}