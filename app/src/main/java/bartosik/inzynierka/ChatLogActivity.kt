package bartosik.inzynierka

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import bartosik.inzynierka.model.Message
import bartosik.inzynierka.model.Profile
import bartosik.inzynierka.model.Ride
import bartosik.inzynierka.util.CircleTransformation
import bartosik.inzynierka.util.DatabaseManager
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.activity_chat_log.toolbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.chat_to_row.view.*
import kotlinx.android.synthetic.main.chat_from_row.view.*
import java.time.Instant

class ChatLogActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val adapter = GroupAdapter<GroupieViewHolder>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_log)
        auth = Firebase.auth
        chatLog.adapter = adapter
        sendMessageButton.setOnClickListener {
            sendMessage()
        }
        listenForMessages()
        toolbar.title = intent.getStringExtra("name")
        toolbar.subtitle = intent.getStringExtra("ride")
        toolbar.setNavigationOnClickListener {
            finish()
        }
        invalidateOptionsMenu()
        DatabaseManager.getRideByUUID(intent.getStringExtra("rideUid")!!).addOnSuccessListener {
            val ride = it.documents[0].toObject<Ride>()
            if (!ride!!.listed){
                toolbar.menu.findItem(R.id.acceptChat).isVisible = false
                toolbar.menu.findItem(R.id.endChat).isVisible = false
            }
            val itemAccept = toolbar.menu.findItem(R.id.acceptChat)
            if (auth.uid == ride.userId){
                itemAccept.isVisible = true
            }
        }
        toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.endChat){
                DatabaseManager.endChat(auth.currentUser!!.uid, intent.getStringExtra("uid")!!)
                finish()
            }
            if (it.itemId == R.id.acceptChat){
                DatabaseManager.acceptUser(intent.getStringExtra("rideUid")!!, intent.getStringExtra("uid")!!)
                DatabaseManager.saveMessage(
                    Message.Builder()
                        .setFromId(auth.currentUser!!.uid)
                        .setToId(intent.getStringExtra("uid")!!)
                        .setText("Przejazd zaakceptowany, będzie widoczny w Twojej historii przejazdów")
                        .setTimestamp(Instant.now().epochSecond)
                        .setRide(intent.getStringExtra("ride")!!)
                        .setRideUid(intent.getStringExtra("rideUid")!!)
                        .create()
                )
                Toast.makeText(this, "Zaakceptowano użytkownika", Toast.LENGTH_LONG).show()
            }
            true

        }
    }

    private fun sendMessage(){
        DatabaseManager.saveMessage(
            Message(
                auth.currentUser!!.uid,
                intent.getStringExtra("uid")!!,
                newMessageText.text.toString(),
                Instant.now().epochSecond,
                intent.getStringExtra("ride")!!,
                intent.getStringExtra("rideUid")!!
            )
        )
        newMessageText.text!!.clear()
    }
    private fun listenForMessages(){
        DatabaseManager.messageListener(auth.uid!!, intent.getStringExtra("uid")!!, adapter, chatLog)
    }
}

class ChatToItem(val text: String, val id: String): Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.chat_to_row
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.fromMessageText.text = text
        DatabaseManager.getUserProfile(id).get().addOnCompleteListener {
            val profile = it.result!!.toObject<Profile>()
            if (profile!!.photoUri!=null){
                Picasso.get().load(Uri.parse(profile.photoUri)).transform(CircleTransformation()).into(viewHolder.itemView.imageFrom)
            }else{
                Picasso.get().load(R.drawable.no_profile_photo).transform(CircleTransformation()).into(viewHolder.itemView.imageFrom)

            }
        }
    }
}

class ChatFromItem(val text: String, val id: String): Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.chat_from_row
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.toMessageText.text = text
        DatabaseManager.getUserProfile(id).get().addOnCompleteListener {
            val profile = it.result!!.toObject<Profile>()
            if (profile!!.photoUri!=null){
                Picasso.get().load(Uri.parse(profile.photoUri)).transform(CircleTransformation()).into(viewHolder.itemView.imageTo)

            }else{
                Picasso.get().load(R.drawable.no_profile_photo).transform(CircleTransformation()).into(viewHolder.itemView.imageTo)

            }
        }
    }
}
