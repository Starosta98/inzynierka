package bartosik.inzynierka.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import bartosik.inzynierka.util.DatabaseManager
import bartosik.inzynierka.model.Profile
import bartosik.inzynierka.R
import bartosik.inzynierka.model.Ride
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_add.*
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [AddFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    var dateDeparture: LocalDateTime = LocalDateTime.MIN
    var dateArrival: LocalDateTime = LocalDateTime.MIN
    override fun onCreate(savedInstanceState: Bundle?) {
        auth = Firebase.auth
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonAdd.setOnClickListener {
            onClickButtonAdd()
        }
        Locale.setDefault(Locale.forLanguageTag("PL"))
        val datePickerDeparture = MaterialDatePicker.Builder.datePicker().build()
        val timePickerDeparture = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .build()
        val datePickerArrival = MaterialDatePicker.Builder.datePicker().build()
        val timePickerArrival = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .build()


        datePickerDeparture.addOnPositiveButtonClickListener {
            dateDeparture = LocalDateTime.ofInstant(Instant.ofEpochMilli(it), TimeZone.getDefault().toZoneId())
            timePickerDeparture.show(fragmentManager!!, timePickerDeparture.toString())
        }
        timePickerDeparture.addOnPositiveButtonClickListener {
            dateDeparture = dateDeparture.plusHours(timePickerDeparture.hour.toLong() -1).plusMinutes(timePickerDeparture.minute.toLong())
            departureAddText.setText(dateDeparture.format(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")))
        }
        departureAddText.setOnClickListener{
            datePickerDeparture.show(fragmentManager!!, datePickerDeparture.toString())
        }


        datePickerArrival.addOnPositiveButtonClickListener {
            dateArrival = LocalDateTime.ofInstant(Instant.ofEpochMilli(it), TimeZone.getDefault().toZoneId())
            timePickerArrival.show(fragmentManager!!, timePickerArrival.toString())
        }
        timePickerArrival.addOnPositiveButtonClickListener {
            dateArrival = dateArrival.plusHours(timePickerArrival.hour.toLong() -1).plusMinutes(timePickerArrival.minute.toLong())
            arrivalAddText.setText(dateArrival.format(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")))
        }
        arrivalAddText.setOnClickListener{
            datePickerArrival.show(fragmentManager!!, datePickerArrival.toString())
        }
    }

    private fun onClickButtonAdd(){
        val rideNumber = trainNumberAdd.editText!!.text.toString()
        DatabaseManager.getUserProfile(auth.currentUser!!.uid).get().addOnSuccessListener {
            val profile = it.toObject<Profile>()
            DatabaseManager.addNewRide(
                Ride(
                    UUID.randomUUID().toString(),
                    rideNumber,
                    Timestamp(dateDeparture.toEpochSecond(ZoneOffset.UTC), 0),
                    Timestamp(dateArrival.toEpochSecond(ZoneOffset.UTC), 0),
                    auth.currentUser!!.displayName.toString(),
                    profile!!.description,
                    auth.currentUser!!.photoUrl.toString(),
                    auth.uid!!
                )
            ).addOnCompleteListener { task ->
                DatabaseManager.getUserProfile(auth.currentUser!!.uid).update("createdRides", FieldValue.arrayUnion(task.result!!.id))
            }
            Toast.makeText(context, "Przejazd został dodany", Toast.LENGTH_LONG).show()
        }

    }

    companion object {

        @JvmStatic
        fun newInstance() = AddFragment()
    }
}