package bartosik.inzynierka.fragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isEmpty
import androidx.fragment.app.Fragment
import bartosik.inzynierka.R
import bartosik.inzynierka.SearchResultsActivity
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import kotlinx.android.synthetic.main.fragment_search.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var dateDeparture: LocalDateTime = LocalDateTime.MIN
    var dateArrival: LocalDateTime = LocalDateTime.MIN
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Locale.setDefault(Locale.forLanguageTag("PL"))
        val datePickerDeparture = MaterialDatePicker.Builder.datePicker().build()
        val timePickerDeparture = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .build()
        val datePickerArrival = MaterialDatePicker.Builder.datePicker().build()
        val timePickerArrival = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .build()


        datePickerDeparture.addOnPositiveButtonClickListener {
            dateDeparture = LocalDateTime.ofInstant(Instant.ofEpochMilli(it), TimeZone.getDefault().toZoneId())
            timePickerDeparture.show(fragmentManager!!, timePickerDeparture.toString())
        }
        timePickerDeparture.addOnPositiveButtonClickListener {
            dateDeparture = dateDeparture.plusHours(timePickerDeparture.hour.toLong() -1).plusMinutes(timePickerDeparture.minute.toLong())
            departureSearchText.setText(dateDeparture.format(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")))
        }
        departureSearchText.setOnClickListener{
            datePickerDeparture.show(fragmentManager!!, datePickerDeparture.toString())
        }


        datePickerArrival.addOnPositiveButtonClickListener {
            dateArrival = LocalDateTime.ofInstant(Instant.ofEpochMilli(it), TimeZone.getDefault().toZoneId())
            timePickerArrival.show(fragmentManager!!, timePickerArrival.toString())
        }
        timePickerArrival.addOnPositiveButtonClickListener {
            dateArrival = dateArrival.plusHours(timePickerArrival.hour.toLong() -1).plusMinutes(timePickerArrival.minute.toLong())
            arrivalSearchText.setText(dateArrival.format(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")))

        }
        arrivalSearchText.setOnClickListener{
            datePickerArrival.show(fragmentManager!!, datePickerArrival.toString())
        }

        buttonSearch.setOnClickListener {
            var correct = true
            if (TextUtils.isEmpty(trainNumberSearch.editText!!.text)){
                correct = false
                trainNumberSearch.error = "Pole wymagane"
            }else{
                trainNumberSearch.error = null
            }
            if (TextUtils.isEmpty(arrivalSearch.editText!!.text)){
                correct = false
                arrivalSearch.error = "Pole wymagane"
            }else{
                if (dateArrival < dateDeparture){
                    correct = false
                    arrivalSearch.error = "Data przyjazdu musi być późniejsza niż data wyjazdu"
                }else{
                    arrivalSearch.error = null
                }
            }

            if (TextUtils.isEmpty(departureSearch.editText!!.text)){
                correct = false
                departureSearch.error = "Pole wymagane"
            }else{
                departureSearch.error = null
            }

            if (correct){
                val i = Intent(activity, SearchResultsActivity::class.java)
                i.putExtra("rideNumber", trainNumberSearch.editText!!.text.toString())
                i.putExtra("departure", dateDeparture.toEpochSecond(ZoneOffset.UTC))
                i.putExtra("arrival", dateArrival.toEpochSecond(ZoneOffset.UTC))
                startActivity(i)
            }

        }

    }
    companion object {
        @JvmStatic
        fun newInstance() = SearchFragment()
    }
}