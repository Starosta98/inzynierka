package bartosik.inzynierka.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import bartosik.inzynierka.ChatFromItem
import bartosik.inzynierka.R
import bartosik.inzynierka.model.LatestMessageRow
import bartosik.inzynierka.model.Message
import bartosik.inzynierka.model.Profile
import bartosik.inzynierka.util.DatabaseManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_chat.*


/**
 * A simple [Fragment] subclass.
 * Use the [ChatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChatFragment : Fragment() {


    val adapter = GroupAdapter<GroupieViewHolder>()
    val latestMessagesMap = HashMap<String, Message>()
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onResume() {
        super.onResume()
        latestMessages.invalidate()
        refreshRecycler()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        latestMessageListener(auth.uid!!, adapter)
        latestMessages.adapter = adapter
        latestMessages.addItemDecoration(
            DividerItemDecoration(
                this.context,
                DividerItemDecoration.VERTICAL
            )
        )

    }

    companion object {
        fun newInstance() = ChatFragment()
    }

    private fun refreshRecycler() {
        adapter.clear()
        latestMessagesMap.values.forEach {
            adapter.add(LatestMessageRow(it))
        }
    }

    private fun latestMessageListener(fromId: String, adapter: GroupAdapter<GroupieViewHolder>) {
        adapter.clear()
        Firebase.database.reference.child("latestMessages/${fromId}").addChildEventListener(object :
            ChildEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val message = snapshot.getValue(Message::class.java)!!
                latestMessagesMap[snapshot.key!!] = message
                refreshRecycler()

            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val message = snapshot.getValue(Message::class.java)!!
                latestMessagesMap[snapshot.key!!] = message
                refreshRecycler()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                latestMessagesMap.remove(snapshot.key)
                refreshRecycler()
            }

        })
//        store.collection(LATEST_MESSAGES).document(fromId).collection(LATEST_MESSAGES)
//            .addSnapshotListener { snapshot, _ ->
//            for (dc in snapshot!!.documentChanges){
//                when(dc.type){
//                    DocumentChange.Type.ADDED -> {
//                        adapter.add(LatestMessageRow(dc.document["name"].toString(), dc.document["ride"].toString(), dc.document["text"].toString(), dc.document["timestamp"]  as Timestamp, dc.document.id, dc.document["uri"].toString()))
//                    }
//                }
//            }
//        }
    }
}


