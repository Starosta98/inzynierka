package bartosik.inzynierka.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import bartosik.inzynierka.EditProfileActivity
import bartosik.inzynierka.HistoryActivity
import bartosik.inzynierka.LoginActivity
import bartosik.inzynierka.R
import bartosik.inzynierka.model.Profile
import bartosik.inzynierka.util.CircleTransformation
import bartosik.inzynierka.util.DatabaseManager
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*


/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {

    private val NEW_PHOTO = 1001;
    private lateinit var storage: FirebaseStorage
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        storage = Firebase.storage
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        activity!!.actionBar!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context!!, R.color.primaryGreen)))
        DatabaseManager.getUserProfile(auth.uid!!).get().addOnSuccessListener {
            val profile = it.toObject<Profile>()!!
            if (profile.photoUri!=null){
                Picasso.get().load(Uri.parse(profile.photoUri)).transform(
                    CircleTransformation()
                ).into(profilePhoto)
            }else{
                Picasso.get().load(R.drawable.no_profile_photo).transform(CircleTransformation()).into(profilePhoto)
            }
        }

        profileName.text = auth.currentUser!!.displayName
        profileEmail.text = auth.currentUser!!.email
        openEditProfile.setOnClickListener {
            val intent = Intent(activity, EditProfileActivity::class.java)
            intent.putExtra("afterRegistration", false)
            startActivity(intent)
            onDestroyView()
        }

        openMyRides.setOnClickListener {
            val intent = Intent(activity, HistoryActivity::class.java)
            startActivity(intent)
        }

        logout.setOnClickListener {
            auth.signOut()
            AccessToken.setCurrentAccessToken(null);
            if (LoginManager.getInstance() != null) {
                LoginManager.getInstance().logOut();
            }
            val intent = Intent(this.context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() = ProfileFragment()
    }
}