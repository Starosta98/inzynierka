package bartosik.inzynierka

import android.annotation.SuppressLint
import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import bartosik.inzynierka.adapter.SearchResultsAdapter
import bartosik.inzynierka.model.Ride
import bartosik.inzynierka.util.DatabaseManager
import bartosik.inzynierka.util.FirebaseSearchResultCallback
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.activity_search_results.*
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class SearchResultsActivity : AppCompatActivity() {

    private lateinit var adapter: SearchResultsAdapter

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)
        DatabaseManager.findRides(
            intent.getStringExtra("rideNumber")!!,
            Timestamp(intent.getLongExtra("departure", 0), 0),
            Timestamp(intent.getLongExtra("arrival", 0), 0),
            object :
                FirebaseSearchResultCallback {
                override fun onCallBack(rides: ArrayList<Ride>) {
                    if (rides.size > 0) {
                        count.text = resources.getQuantityString(
                            R.plurals.numberOfSearchResults,
                            rides.size,
                            rides.size
                        )
                        imageView.visibility = View.VISIBLE
                        imageViewRed.visibility = View.INVISIBLE
                        trainIcon.visibility = View.VISIBLE
                        trainIconRed.visibility = View.INVISIBLE
                    } else {
                        count.text =
                            "Nie znaleziono pociagu o podanych danych.\nSprawdź inne połączenie"
                        trainNumberList.setTextColor(resources.getColor(R.color.red))
                        imageView.visibility = View.INVISIBLE
                        imageViewRed.visibility = View.VISIBLE
                        trainIcon.visibility = View.INVISIBLE
                        trainIconRed.visibility = View.VISIBLE
                    }
                    adapter = SearchResultsAdapter(
                        rides,
                        intent.getLongExtra("departure", 0),
                        intent.getLongExtra("arrival", 0)
                    )
                    recyclerSearch.adapter = adapter
                    recyclerSearch.layoutManager = LinearLayoutManager(this@SearchResultsActivity)
                    recyclerSearch.hasFixedSize()
                }

            })

        trainNumberList.text = intent.getStringExtra("rideNumber")!!
        ride.text =
            LocalDateTime.ofEpochSecond(intent.getLongExtra("departure", 0), 0, ZoneOffset.UTC)
                .format(
                    DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")
                ) + " - " + LocalDateTime.ofEpochSecond(
                intent.getLongExtra("arrival", 0),
                0,
                ZoneOffset.UTC
            ).format(
                DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")
            )


        toolbar.setNavigationOnClickListener {
            finish()
        }


    }
}