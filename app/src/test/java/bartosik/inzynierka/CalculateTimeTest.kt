package bartosik.inzynierka

import bartosik.inzynierka.model.Ride
import bartosik.inzynierka.util.CommonTimeHelper
import com.google.firebase.Timestamp
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class CalculateTimeTest {
    @Test
    fun calculateCommonTime(){
        val ride = Ride()
        val myTimeArrive = getTimestampFromDateString("03.12.2020 10:15")
        val myTimeDeparture = getTimestampFromDateString("03.12.2020 15:30")

        //Przypadek 1
        ride.departureTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 09:15"), 0)
        ride.arrivalTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 14:15"), 0)
        assertEquals( 14400, CommonTimeHelper.countCommonTime(ride, myTimeDeparture, myTimeArrive))

        //Przypadek 2
        ride.departureTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 09:15"), 0)
        ride.arrivalTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 16:15"), 0)
        assertEquals( 18900, CommonTimeHelper.countCommonTime(ride, myTimeDeparture, myTimeArrive))

        //Przypadek 3
        ride.departureTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 11:15"), 0)
        ride.arrivalTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 16:15"), 0)
        assertEquals( 15300, CommonTimeHelper.countCommonTime(ride, myTimeDeparture, myTimeArrive))

        //Przypadek 4
        ride.departureTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 11:15"), 0)
        ride.arrivalTimestamp = Timestamp(getTimestampFromDateString("03.12.2020 14:15"), 0)
        assertEquals( 10800, CommonTimeHelper.countCommonTime(ride, myTimeDeparture, myTimeArrive))

    }

    private fun getTimestampFromDateString(date: String): Long{
        val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
        return LocalDateTime.parse(date, formatter).toEpochSecond(ZoneOffset.UTC)
    }
}